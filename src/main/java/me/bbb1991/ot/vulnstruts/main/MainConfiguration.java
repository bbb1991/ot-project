package me.bbb1991.ot.vulnstruts.main;

import me.bbb1991.ot.vulnstruts.dao.CategoryDao;
import me.bbb1991.ot.vulnstruts.dao.CommentDao;
import me.bbb1991.ot.vulnstruts.dao.PersonDao;
import me.bbb1991.ot.vulnstruts.dao.PostDao;
import me.bbb1991.ot.vulnstruts.service.CategoryService;
import me.bbb1991.ot.vulnstruts.service.CommentService;
import me.bbb1991.ot.vulnstruts.service.PersonService;
import me.bbb1991.ot.vulnstruts.service.PostService;
import org.apache.struts2.dispatcher.filter.StrutsPrepareAndExecuteFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 March 2018
 */
@Configuration
public class MainConfiguration {

    @Bean
    public CategoryDao getCategoryDao() {
        return new CategoryDao();
    }

    @Bean
    public CommentService getCommentService() {
        return new CommentService();
    }

    @Bean
    public CommentDao getCommentDao() {
        return new CommentDao();
    }

    @Bean
    public PersonDao getPersonDao() {
        return new PersonDao();
    }

    @Bean
    public PostDao getPostDao() {
        return new PostDao();
    }

    @Bean
    public PersonService getPersonService() {
        return new PersonService();
    }

    @Bean
    public PostService getNoteService() {
        return new PostService();
    }

    @Bean
    public CategoryService getCategoryService() {
        return new CategoryService();
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean<StrutsPrepareAndExecuteFilter> registrationBean = new FilterRegistrationBean<>();
        StrutsPrepareAndExecuteFilter struts = new StrutsPrepareAndExecuteFilter();
        registrationBean.setFilter(struts);
        registrationBean.setOrder(1);
        return registrationBean;
    }
}
