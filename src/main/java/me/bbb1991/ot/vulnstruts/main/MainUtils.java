package me.bbb1991.ot.vulnstruts.main;

import javax.servlet.http.HttpServletRequest;
import java.util.StringTokenizer;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 22 March 2018
 */

public class MainUtils {

    public static String getClientIp(HttpServletRequest request) {
        String xForwardedForHeader = request.getHeader("X-Forwarded-For");
        if (xForwardedForHeader == null) {
            return request.getRemoteAddr();
        } else {
            // As of https://en.wikipedia.org/wiki/X-Forwarded-For
            // The general format of the field is: X-Forwarded-For: client, proxy1, proxy2 ...
            // we only want the client
            return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
        }

    }

}
