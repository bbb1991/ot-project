package me.bbb1991.ot.vulnstruts.dao;

import me.bbb1991.ot.vulnstruts.model.Comment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */
@Transactional
@Repository
public class CommentDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void save(Comment comment) {
        entityManager.persist(comment);
    }

    @SuppressWarnings("unchecked")
    public List<Comment> getAll() {
        Query query = entityManager.createQuery("select c from Comment c");
        List result = query.getResultList();
        return Collections.checkedList(result, Comment.class);
    }
}
