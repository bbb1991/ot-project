package me.bbb1991.ot.vulnstruts.dao;

import me.bbb1991.ot.vulnstruts.model.Category;
import me.bbb1991.ot.vulnstruts.model.Post;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */

@Repository
@Transactional
public class PostDao {


    @PersistenceContext
    private EntityManager entityManager;


    public Post getById(Integer id) {
        return entityManager.find(Post.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Post> getAll() {
        List result = entityManager.createQuery("from Post").getResultList();

        return Collections.checkedList(result, Post.class);
    }


    public void save(Post post) {
        entityManager.persist(post);
    }

    @SuppressWarnings("unchecked")
    public List<Post> getByText(String text) {
        Query query = entityManager.createQuery("select p from Post p where p.body like :text");
        query.setParameter("text", "%" + text + "%");
        List result = query.getResultList();
        return Collections.checkedList(result, Post.class);
    }

    @SuppressWarnings("unchecked")
    public List<Post> getByCategory(Category category) {
        Query query = entityManager.createQuery("select p from Post p where p.category = :category");
        query.setParameter("category", category);
        List result = query.getResultList();

        return Collections.checkedList(result, Post.class);
    }
}
