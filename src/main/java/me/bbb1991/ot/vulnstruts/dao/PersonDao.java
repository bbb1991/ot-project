package me.bbb1991.ot.vulnstruts.dao;

import me.bbb1991.ot.vulnstruts.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */

@Repository
@Transactional
public class PersonDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Person getById(Integer id) {
        try {
            return entityManager.find(Person.class, id);
        } catch (NoResultException ex) {
            return null;
        }
    }

    public Person getByUsername(String username) {
        try {
            LOGGER.info("Executing query to get user {} from DB" , username);
            Query query = entityManager.createQuery("select p from Person p where p.username = :username");
            query.setParameter("username", username);
            Person person = (Person) query.getSingleResult();
            LOGGER.info("Got person: {}", person);
            return person;
        } catch (NoResultException exception) {
            LOGGER.warn("Person with username {} does not exist!", username);
            return null;
        }
    }

    public void addPerson(Person person) {
        entityManager.persist(person);
    }
}
