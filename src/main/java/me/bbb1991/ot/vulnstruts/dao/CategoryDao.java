package me.bbb1991.ot.vulnstruts.dao;

import me.bbb1991.ot.vulnstruts.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */

@Repository
@Transactional
public class CategoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Category getByName(String name) {
        return (Category) entityManager.createQuery("from Category").getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<Category> getAll() {
        Query query = entityManager.createQuery("from Category");
        List result = query.getResultList();
        return Collections.checkedList(result, Category.class);
    }

    public void save(Category category) {
        entityManager.persist(category);
    }

    public Category getbyId(int categoryId) {
        return entityManager.find(Category.class, categoryId);
    }
}
