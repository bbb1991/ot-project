package me.bbb1991.ot.vulnstruts.service;

import me.bbb1991.ot.vulnstruts.dao.PersonDao;
import me.bbb1991.ot.vulnstruts.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Optional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */

@Service
//@PropertySource("classpath:application.properties")
public class PersonService {

    //    @Value("${superuser.username}")
    private String superuserUsername = "elvis";

    //    @Value("${superuser.password}")
    private String superuserPassword = "admin";

    private static final int ITERATIONS = 12;

    private static final String SALT = "struts";

    private static final int KEY_LENGTH = 256;

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    private PersonDao personDao;

    @PostConstruct
    public void init() {
        LOGGER.info("Creating new superuser in DB. Username: {}", superuserUsername);
        String hashedPassword = encryptPassword(superuserPassword);

        try {
            LOGGER.info("PersonDAO: {}", personDao);
            personDao.addPerson(new Person(superuserUsername, hashedPassword, superuserUsername + "@sne.com", "John", "Smith"));
        } catch (DataIntegrityViolationException ex) {
            LOGGER.warn("Superuser already exist! Additional message: {}", ex.getMessage());
        }
    }

    public boolean validateLogin(final String username, final String password) {
        String hashedPassword = encryptPassword(password);
        Optional<Person> optionalPerson = getByUsername(username);
        if (!optionalPerson.isPresent()) {
            LOGGER.warn("User with username {} does not exist!", username);
            return false;
        }
        LOGGER.info("We are expecting: {}, given: {}", hashedPassword, optionalPerson.get().getPassword());
        boolean result = optionalPerson.map(person -> person.getPassword().equals(hashedPassword)).orElse(false);
        LOGGER.info("Result of validating is: {}", result);
        return result;
    }

    @Autowired
    public void setPersonDao(PersonDao personDao) {
        this.personDao = personDao;
    }

    public Optional<Person> getByUsername(String username) {
        return Optional.ofNullable(personDao.getByUsername(username));
    }

    public Person getById(Integer id) {
        return personDao.getById(id);
    }

    private String encryptPassword(final String password) {

        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), SALT.getBytes(), ITERATIONS, KEY_LENGTH);
            SecretKey key = skf.generateSecret(spec);
            return new String(key.getEncoded());

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }
}
