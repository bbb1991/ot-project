package me.bbb1991.ot.vulnstruts.service;

import me.bbb1991.ot.vulnstruts.dao.PostDao;
import me.bbb1991.ot.vulnstruts.model.Category;
import me.bbb1991.ot.vulnstruts.model.Post;
import me.bbb1991.ot.vulnstruts.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 19 March 2018
 */

@Service
public class PostService {

    private PostDao postDao;

    public List<Post> getAllPosts() {
        return postDao.getAll();
    }

    public Optional<Post> getPostById(Integer id) {
        return Optional.ofNullable(postDao.getById(id));
    }

    @Autowired
    public void setPostDao(PostDao postDao) {
        this.postDao = postDao;
    }

    public void savePost(Post post) {
        postDao.save(post);
    }

    public List<Post> getPostByText(String search) {
        return postDao.getByText(search);
    }

    public List<Post> getPostByCategory(Category category) {
        return postDao.getByCategory(category);
    }
}
