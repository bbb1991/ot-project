package me.bbb1991.ot.vulnstruts.service;

import me.bbb1991.ot.vulnstruts.dao.CommentDao;
import me.bbb1991.ot.vulnstruts.model.Comment;
import me.bbb1991.ot.vulnstruts.model.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */
@Service
public class CommentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentService.class);

    private PostService postService;

    private CommentDao commentDao;


    public boolean saveComment(int postId, Comment comment) {
        Optional<Post> post = postService.getPostById(postId);
        if (post.isPresent()) {
            comment.setPost(post.get());
            commentDao.save(comment);
            return true;
        }
        return false;
    }

    @Autowired
    public void setPostService(PostService postService) {
        this.postService = postService;
    }

    @Autowired
    public void setCommentDao(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    public List<Comment> getAllComments() {
        List<Comment> comments = this.commentDao.getAll();
        LOGGER.info("Got comments: Elements in list: {}", comments.size());
        return comments;
    }
}
