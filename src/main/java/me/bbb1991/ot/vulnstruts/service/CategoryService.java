package me.bbb1991.ot.vulnstruts.service;

import me.bbb1991.ot.vulnstruts.dao.CategoryDao;
import me.bbb1991.ot.vulnstruts.model.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */

@Service
public class CategoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryService.class);

    private CategoryDao categoryDao;

    @PostConstruct
    public void init() {
        try {
            categoryDao.save(new Category("uncategorized"));
        } catch (DataIntegrityViolationException ex) {
            LOGGER.warn("Category 'uncategorized' already exist!, Additional message: {}", ex.getMessage());
        }
    }

    public List<Category> getAllCategories() {
        return categoryDao.getAll();
    }

    @Autowired
    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    public Optional<Category> getCategoryByName(String name) {
        try {
            return Optional.ofNullable(categoryDao.getByName(name));
        } catch (NoResultException ex) {
            LOGGER.warn("Category with name {} does not found in DB!", name);
            return Optional.empty();
        }
    }

    public void saveCategory(Category category) {
        categoryDao.save(category);
    }

    public Optional<Category> getCategoryById(int id) {
        try {
            return Optional.ofNullable(categoryDao.getbyId(id));
        } catch (NoResultException ex) {
            LOGGER.warn("Category with ID {} does not found in DB!", id);
            return Optional.empty();
        }
    }
}
