package me.bbb1991.ot.vulnstruts.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import me.bbb1991.ot.vulnstruts.main.MainUtils;
import me.bbb1991.ot.vulnstruts.model.Category;
import me.bbb1991.ot.vulnstruts.model.Comment;
import me.bbb1991.ot.vulnstruts.model.Person;
import me.bbb1991.ot.vulnstruts.model.Post;
import me.bbb1991.ot.vulnstruts.service.CategoryService;
import me.bbb1991.ot.vulnstruts.service.CommentService;
import me.bbb1991.ot.vulnstruts.service.PersonService;
import me.bbb1991.ot.vulnstruts.service.PostService;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 19 March 2018
 */

@Component
public class PostAction extends ActionSupport implements Preparable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostAction.class);

    private PostService postService;

    private PersonService personService;

    private CommentService commentService;

    private CategoryService categoryService;

    private List<Post> posts = new ArrayList<>();

    private List<Category> categories = new ArrayList<>();

    private String id;

    private Post post;

    private String givenCategory;

    private List<Comment> comments = new ArrayList<>();

    private String search;

    public String getPostById() {

        LOGGER.info("In PostAction.getPostById() method. Request from:  {}", MainUtils.getClientIp(ServletActionContext.getRequest()));

        int postId;

        try {
            postId = Integer.parseInt(id);
            if (postId <= 0) {
                throw new RuntimeException("Incorrect ID passed! Passed ID: " + postId);
            }
        } catch (Exception ex) {
            LOGGER.error("Exception when trying to parse post id! Message: {}", ex.getMessage());
            return ERROR;
        }

        Optional<Post> optionalPost = postService.getPostById(postId);
        if (optionalPost.isPresent()) {
            post = optionalPost.get();
            return SUCCESS;
        }
        return ERROR;
    }

    @Override
    public String execute() {
        LOGGER.info("In PostAction.execute() method (Main page). Request from: {}", MainUtils.getClientIp(ServletActionContext.getRequest()));
        return SUCCESS;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Autowired
    public void setPostService(PostService postService) {
        this.postService = postService;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public void prepare() throws Exception {
        HttpServletResponse response = ServletActionContext.getResponse();
        response.addHeader("x-powered-by", "PHP/5.5.31");
        response.addHeader("Server", "Apache/2.3.6 (CentOS) PHP/5.5.31 OpenSSL/1.1.0a Microsoft-HTTPAPI/2.0 Communique/4.2.2");
        response.addHeader("via", "1.1 varnish");
        posts = postService.getAllPosts();
        categories = categoryService.getAllCategories();
        comments = commentService.getAllComments();

        Collections.reverse(posts);
        Collections.reverse(categories);
        Collections.reverse(comments);
    }

    public String checkLoggedIn() {
        HttpSession session = ServletActionContext.getRequest().getSession(true);
        Object o = session.getAttribute("userId");
        LOGGER.info("User ID is: {}", o);
        return o == null ? ERROR : SUCCESS;
    }


    public String addNewPost() {

        LOGGER.info("In PostAction.addNewPost() method. Request from:  {}", MainUtils.getClientIp(ServletActionContext.getRequest()));
        // if user does not logged in just skip it
        if (checkLoggedIn().equals(ERROR)) {
            return SUCCESS;
        }

        String method = ServletActionContext.getRequest().getMethod();
        if (!"POST".equalsIgnoreCase(method)) { // ACCEPT only post requests. If not POST, just skip it.
            return SUCCESS;
        }

        LOGGER.info("Saving new Post");
        HttpSession session = ServletActionContext.getRequest().getSession(true);
        Object o = session.getAttribute("userId");
        Integer id = (int) o;
        LOGGER.info("ID is: {}", id);
        Person person = personService.getById(id);
        LOGGER.info("Got person: {}", person);
        post.setAuthor(person);

        if (givenCategory.trim().isEmpty()) {
            Category category = categoryService.getCategoryByName("uncategorized").get();
            post.setCategory(category);
        } else {
            LOGGER.info("Checking if category exist or not");
            Optional<Category> optionalCategory = categoryService.getCategoryByName(givenCategory);

            Category category = optionalCategory.orElseGet(() -> {
                Category newCategory = new Category(givenCategory);
                categoryService.saveCategory(newCategory);
                return newCategory;
            });

            post.setCategory(category);
        }

        LOGGER.info("Saving post with title: {}", post.getTitle());

        postService.savePost(post);

        return SUCCESS;
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    public String getGivenCategory() {
        return givenCategory;
    }

    public void setGivenCategory(String givenCategory) {
        this.givenCategory = givenCategory;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String doSearch() {

        LOGGER.info("In PostAction.doSearch() method. Request from:  {}", MainUtils.getClientIp(ServletActionContext.getRequest()));
        LOGGER.info("SEARCH IS: {}", search);
        posts = postService.getPostByText(search);
        Collections.reverse(posts);
        return SUCCESS;
    }

    public String filterByCategory() throws Exception {

        LOGGER.info("In PostAction.filterByCategory() method. Request from:  {}", MainUtils.getClientIp(ServletActionContext.getRequest()));

        int categoryId;

        try {
            categoryId = Integer.parseInt(id);
            if (categoryId <= 0) {
                throw new RuntimeException("Incorrect ID passed! Passed ID: " + categoryId);
            }
        } catch (Exception ex) {
            LOGGER.error("Exception when trying to parse category id! Message: {}", ex.getMessage());
            return ERROR;
        }

        Optional<Category> optionalCategory = categoryService.getCategoryById(categoryId);
        if (optionalCategory.isPresent()) {
            posts = postService.getPostByCategory(optionalCategory.get());
            Collections.reverse(posts);
            return SUCCESS;
        }
        return ERROR;
    }
}
