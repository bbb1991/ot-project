package me.bbb1991.ot.vulnstruts.action;

import com.opensymphony.xwork2.ActionSupport;
import me.bbb1991.ot.vulnstruts.main.MainUtils;
import me.bbb1991.ot.vulnstruts.service.PersonService;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */

@Component
public class LoginAction extends ActionSupport implements SessionAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginAction.class);

    private String username;

    private String password;

    private boolean flag = false;

    private SessionMap<String, Object> sessionMap;

    private PersonService personService;

    @Override
    public void setSession(Map<String, Object> map) {
        LOGGER.info("In setSession method");
        sessionMap = (SessionMap<String, Object>) map;
    }

    @Override
    public String execute() {

        LOGGER.warn("Someone trying to log in!");
        LOGGER.info("In LoginAction.execute() method. Request from:  {}", MainUtils.getClientIp(ServletActionContext.getRequest()));

        String method = ServletActionContext.getRequest().getMethod();
        if (!"POST".equalsIgnoreCase(method)) { // ACCEPT only post requests. If not POST, just skip it.
            return SUCCESS;
        }
        LOGGER.info("Incoming request for log in. Username: {}, password: {}", username, password);
        boolean validCredentials = personService.validateLogin(username, password);
        if (validCredentials) {
            sessionMap.put("userId", personService.getByUsername(username).get().getId());
            LOGGER.info("Everything is OK. User {} successfully logged in.", username);
            return SUCCESS;
        }
        LOGGER.warn("Something went wrong. User {} does not logged in!", username);
        flag = true;
        return ERROR;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String logout() {

        LOGGER.info("In LoginAction.logout() method. Request from:  {}", MainUtils.getClientIp(ServletActionContext.getRequest()));

        sessionMap.invalidate();
        return SUCCESS;
    }

    @Autowired
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }
}
