package me.bbb1991.ot.vulnstruts.action;

import com.opensymphony.xwork2.ActionSupport;
import me.bbb1991.ot.vulnstruts.main.MainUtils;
import me.bbb1991.ot.vulnstruts.model.Comment;
import me.bbb1991.ot.vulnstruts.service.CommentService;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Put your awesome documentation here...
 *
 * @author Bagdat Bimaganbetov
 * @author bagdat.bimaganbetov@gmail.com
 * @version 1.0, 20 March 2018
 */
@Component
public class CommentAction extends ActionSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentAction.class);

    private Comment comment;

    private CommentService commentService;

    private String postId;

    @Override
    public String execute() throws Exception {

        LOGGER.info("Someone tries to add new comment!");
        LOGGER.info("In CommentAction.execute() method. Request from:  {}", MainUtils.getClientIp(ServletActionContext.getRequest()));

        String method = ServletActionContext.getRequest().getMethod();
        if (!"POST".equalsIgnoreCase(method)) { // ACCEPT only post requests. If not POST, just skip it.
            return SUCCESS;
        }

        // if required fields are empty then just skip
        if (comment.getEmail().trim().isEmpty() || comment.getName().trim().isEmpty()) {
            return SUCCESS;
        }

        int id;
        try {
            id = Integer.parseInt(postId);
        } catch (Exception ex) {
            LOGGER.error("Incorrect post id were given! ID: {}", postId);
            return ERROR;
        }

        boolean isSuccess = commentService.saveComment(id, comment);

        return isSuccess ? SUCCESS : ERROR;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
}
