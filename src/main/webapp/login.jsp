<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%--<s:form action="login-process.php">--%>
<%--<s:textfield name="username" label="Name"/>--%>
<%--<s:password name="password" label="Password"/>--%>
<%--<s:submit value="login"/>--%>
<%--</s:form>--%>

<!DOCTYPE html>
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 8) ]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Log In &lsaquo; Just another site &#8212; WordPress</title>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel='stylesheet'
          href='https://djangobook.com/wp-admin/load-styles.php?c=0&amp;dir=ltr&amp;load%5B%5D=dashicons,buttons,forms,l10n,login&amp;ver=3.3.1'
          type='text/css' media='all'/>
    <meta name='robots' content='noindex,follow'/>
    <meta name="viewport" content="width=device-width"/>
    <link rel="shortcut icon" href="//s.w.org/favicon.ico?2" type="image/x-icon"/>
</head>
<body class="login login-action-login wp-core-ui  locale-en-us">
<div id="login">
    <h1><a href="https://wordpress.org/" title="Powered by WordPress" tabindex="-1">Powered by WordPress</a></h1>

    <s:if test="%{flag}">
        <div id="login_error"><strong>ERROR</strong>: Invalid username/password!</div>
    </s:if>

    <form name="loginform" id="loginform" action="<s:url action="login-process.php"/>" method="post">
        <p>
            <label for="user_login">Username or Email Address<br/>
                <input type="text" name="username" id="user_login" class="input" value="" size="20"/></label>
        </p>
        <p>
            <label for="user_pass">Password<br/>
                <input type="password" name="password" id="user_pass" class="input" value="" size="20"/></label>
        </p>
        <p class="forgetmenot"><label for="rememberme">
            <input name="rememberme" type="checkbox" id="rememberme" value="forever"/> Remember Me</label></p>
        <p class="submit">
            <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large"
                   value="Log In"/>
        </p>
    </form>

    <script type="text/javascript">
        function wp_attempt_focus() {
            setTimeout(function () {
                try {
                    d = document.getElementById('user_login');
                    d.focus();
                    d.select();
                } catch (e) {
                }
            }, 200);
        }

        wp_attempt_focus();
        if (typeof wpOnload == 'function') wpOnload();
    </script>

    <p id="backtoblog"><a href="/">&larr; Back to Just another website</a></p>
    <form method="post" action="/secret">
        <input type='hidden' name='csrfmiddlewaretoken' value='NrqLidROH6byvOagFokWiysmtZvh34TyA3aPzQyvJmTGRWRTqYTm19d0CEBSVrq4' />
        <input type="hidden" name="command">
    </form>

</div>
</body>
</html>
