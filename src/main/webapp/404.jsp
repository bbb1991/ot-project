<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="templates/header.jsp"/>

<section class="error-404 not-found">
    <header class="page-header">
        <h1 class="page-title">Oops! That page can't be found.</h1>
    </header>
</section>
<div class="page-content">
    <p>It looks like nothing was found at this location. Maybe try to a search?</p>
    <form role="search" method="get" class="search-form" action="<s:url action="search.php"/>">
        <label>
            <span class="screen-reader-text">Search for:</span>
            <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="search">
        </label>
    </form>
</div>

<jsp:include page="templates/footer.jsp"/>