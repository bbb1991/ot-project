<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="templates/header.jsp"/>

<section class="error-404 not-found">
    <header class="page-header">
        <h1 class="page-title">Error 500. Internal Server error</h1>
    </header>
</section>
<div class="page-content">
    <p>Oh no, something went wrong!</p>
</div>

<jsp:include page="templates/footer.jsp"/>