<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="templates/header.jsp"/>

<s:iterator value="posts">
    <article id="post-1"
             class="post-1 post type-post status-publish format-standard hentry category-uncategorized">
        <header class="entry-header">
            <h2 class="entry-title">
                <a href="<s:url action="post.php"><s:param name="id"><s:property value="id"/></s:param></s:url>"><s:property
                        value="title"/></a></h2></header><!-- .entry-header -->
        <div class="entry-content"><p><s:property value="body.substring(0,100) "/></p></div><!-- .entry-content -->

        <footer class="entry-footer">
                            <span class="byline"><span class="author vcard">
                                <img alt=''
                                     src='https://secure.gravatar.com/avatar/407fb36bbf6a01e151680be9f408b5af?s=49&#038;d=identicon&#038;r=pg'
                                     srcset='https://secure.gravatar.com/avatar/407fb36bbf6a01e151680be9f408b5af?s=98&#038;d=identicon&#038;r=pg 2x'
                                     class='avatar avatar-49 photo'
                                     height='49' width='49'/><span
                                    class="screen-reader-text">Author </span>
                                <a class="url fn n"
                                   href="#"><s:property value="author.firstname"/> <s:property value="author.lastname"/></a></span></span><span
                class="posted-on"><span class="screen-reader-text">Posted on </span>
            <time class="entry-date published" datetime="<s:property value="createdAt"/>"><s:property value="createdAt"/> </time><time
                class="updated"
                datetime="2017-08-24T17:46:58+00:00"><s:property value="createdAt"/></time></span><span
                class="cat-links"><span class="screen-reader-text">Categories </span><a
                href="<s:url action="category.php"><s:param name="id"><s:property value="category.id"/></s:param></s:url>"><s:property
                value="category.name"/></a></span><span
                class="comments-link"><a
                href="<s:url action="post.php"><s:param name="id"><s:property value="id"/></s:param></s:url>#comments"><s:property
                value="comments.size()"/> Comments<span
                class="screen-reader-text"> on Hello world!</span></a></span></footer>
        <!-- .entry-footer -->
    </article>
</s:iterator>

<jsp:include page="templates/footer.jsp"/>