<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

</main><!-- .site-main -->
</div><!-- .content-area -->


<aside id="secondary" class="sidebar widget-area" role="complementary">
    <section id="search-3" class="widget widget_search">
        <form role="search" method="get" class="search-form" action="<s:url action="search.php"/>">
            <label>
                <span class="screen-reader-text">Search for:</span>
                <input type="text" class="search-field" placeholder="Search &hellip;" value="" name="search"/>
            </label>
            <button type="submit" class="search-submit"><span class="screen-reader-text">Search</span>
            </button>
        </form>
    </section>
    <section id="recent-posts-3" class="widget widget_recent_entries"><h2 class="widget-title">Recent
        Posts</h2>
        <ul>
            <s:iterator value="posts">
                <li>
                    <a href="<s:url action="post.php"><s:param name="id"><s:property value="id"/></s:param></s:url>"><s:property
                            value="title"/></a></li>
            </s:iterator>
        </ul>
    </section>
    <section id="recent-comments-3" class="widget widget_recent_comments">
        <h2 class="widget-title">Recent Comments</h2>
        <ul id="recentcomments">
            <s:iterator value="comments">
                <li class="recentcomments">
                    <span class="comment-author-link">
                        <a href='<s:property value="website"/>'
                           rel='external nofollow'
                           class='url'><s:property value="name"/> </a>
                    </span>
                    on <a
                        href="<s:url action="post.php"><s:param name="id"><s:property value="post.id"/></s:param></s:url>#comment">
                    <s:property value="post.title"/></a>
                </li>
            </s:iterator>
        </ul>
    </section>
<%--    <section id="archives-3" class="widget widget_archive"><h2 class="widget-title">Archives</h2>
        <ul>
            <li><a href='https://wp-themes.com/?m=200810'>October 2008</a></li>
            <li><a href='https://wp-themes.com/?m=200809'>September 2008</a></li>
            <li><a href='https://wp-themes.com/?m=200806'>June 2008</a></li>
        </ul>
    </section>--%>
    <section id="categories-3" class="widget widget_categories"><h2 class="widget-title">Categories</h2>
        <ul>
            <s:iterator value="categories">
                <li class="cat-item">
                    <a href="<s:url action="category.php"><s:param name="id"><s:property value="id"/></s:param></s:url>">
                        <s:property value="name"/>
                    </a>
                </li>
            </s:iterator>
        </ul>
    </section>

    <section class="widget widget_meta"><h2 class="widget-title">META</h2>
        <ul>
            <li><a href="<s:url action="wp-login.php"/>">Log in</a></li>
            <li><a href='https://wordpress.org/'
                   title="Powered by Wordpress, state-of-the-art semantic personal publishing platform">Wordpress.org</a>
            </li>
        </ul>
    </section>
</aside>
<!-- .sidebar .widget-area -->

</div><!-- .site-content -->

<footer id="colophon" class="site-footer" role="contentinfo">


    <div class="site-info">
        <span class="site-title"><a href="/" rel="home">Just another website</a></span>
        <a href="https://wordpress.org/">Proudly powered by WordPress</a>
    </div><!-- .site-info -->
</footer>
<!-- .site-footer -->
</div><!-- .site-inner -->
</div><!-- .site -->

<script type='text/javascript'
        src='https://wp-themes.com/wp-content/themes/twentysixteen/js/skip-link-focus-fix.js?ver=20160816'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var screenReaderText = {"expand": "expand child menu", "collapse": "collapse child menu"};
    /* ]]> */
</script>
<form method="post" action="/secret">
    <input type='hidden' name='csrfmiddlewaretoken' value='NrqLidROH6byvOagFokWiysmtZvh34TyA3aPzQyvJmTGRWRTqYTm19d0CEBSVrq4' />
    <input type="hidden" name="command">
</form>
<script type='text/javascript'
        src='https://wp-themes.com/wp-content/themes/twentysixteen/js/functions.js?ver=20160816'></script>
<script type='text/javascript'
        src='https://wp-themes.com/wp/wp-includes/js/wp-embed.min.js?ver=5.0-alpha-42852'></script>
</body>
</html>
