<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="templates/header.jsp"/>
<article id="post-1" class="post status-publish format-standard hentry">
    <header class="entry-header">
        <h1 class="entry-title"><s:property value="post.title"/></h1>
        <div class="entry-content">
            <p><s:property value="post.body"/></p>
        </div>
    </header>
</article>


<div id="comments" class="comments-area">
    <h2 class="comments-title">Comment for &ldquo;<s:property value="post.title"/>&rdquo; </h2>


    <ol class="comment-list">
        <s:iterator value="post.comments">
            <li id="comment-1" class="comment even thread-even depth-1">
                <article id="div-comment-1" class="comment-body">
                    <footer class="comment-meta">
                        <div class="comment-author vcard">
                            <img alt=''
                                 src='http://1.gravatar.com/avatar/d7a973c7dab26985da5f961be7b74480?s=42&#038;d=mm&#038;r=g'
                                 srcset='http://1.gravatar.com/avatar/d7a973c7dab26985da5f961be7b74480?s=84&#038;d=mm&#038;r=g 2x'
                                 class='avatar avatar-42 photo' height='42' width='42'/> <b class="fn"><a
                                href='<s:property value="website"/>' rel='external nofollow' class='url'><s:property
                                value="name"/> </a></b>
                            <span class="says">says:</span></div><!-- .comment-author -->

                        <div class="comment-metadata">
                                    <s:property value="createdAt"/>
                        </div><!-- .comment-metadata -->

                    </footer><!-- .comment-meta -->

                    <div class="comment-content">
                        <p><s:property value="body"/></p>
                    </div><!-- .comment-content -->
                </article><!-- .comment-body -->
            </li>
            <!-- #comment-## -->
        </s:iterator>
    </ol><!-- .comment-list -->


    <div id="respond" class="comment-respond">
        <h2 id="reply-title" class="comment-reply-title">Leave a Reply
            <small>
                <a rel="nofollow" id="cancel-comment-reply-link" href="#respond" style="display:none;">Cancel reply</a>
            </small>
        </h2>
        <form action="wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="" accept-charset="UTF-8">
            <p class="comment-notes">
                <span id="email-notes">Your email address will not be published.</span> Required fields are marked
                <span class="required">*</span>
            </p>
            <p class="comment-form-comment">
                <label for="comment">Comment</label>
                <textarea id="comment" name="comment.body" cols="45" rows="8" maxlength="65525" aria-required="true"
                          required></textarea>
            </p>
            <p class="comment-form-author">
                <label for="author">Name <span class="required">*</span></label>
                <input id="author" name="comment.name" type="text" value="" size="30" maxlength="245"
                             aria-required='true' required/>
            </p>
            <p class="comment-form-email">
                <label for="email">Email <span class="required">*</span></label>
                <input id="email" name="comment.email" type="email" value="" size="30" maxlength="100"
                             aria-describedby="email-notes" aria-required='true' required/>
            </p>
            <p class="comment-form-url">
                <label for="url">Website</label>
                <input id="url" name="comment.website" type="url" value="" size="30" maxlength="200"/>
            </p>
            <p class="form-submit">
                <input type="submit" class="submit" value="Post Comment"/>
                <s:textfield type='hidden' name='postId' value='%{post.id}' id='comment_post_ID'/>
            </p>
        </form>
    </div>


</div>
<!-- .comments-area -->


<jsp:include page="templates/footer.jsp"/>