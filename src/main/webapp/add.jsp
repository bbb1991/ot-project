<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="templates/header.jsp"/>

<form action="add-process.php" method="post" id="commentform" class="comment-form" novalidate="" accept-charset="UTF-8">
    <label for="post.title">Post title *</label>
    <input name="post.title" type="text" id="post.title" required>

    <label for="post.body">Post body *</label>
    <textarea name="post.body" id="post.body" cols="30" rows="10" required></textarea>

    <s:label for="categories">Categories:</s:label>
    <input name="givenCategory" type="text" id="categories">
    <s:submit/>
</form>


<jsp:include page="templates/footer.jsp"/>