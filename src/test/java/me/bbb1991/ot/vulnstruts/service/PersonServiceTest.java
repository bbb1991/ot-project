package me.bbb1991.ot.vulnstruts.service;

import me.bbb1991.ot.vulnstruts.dao.PersonDao;
import me.bbb1991.ot.vulnstruts.model.Person;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.el.MethodNotFoundException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PersonService.class})
public class PersonServiceTest {

    @Autowired
    private PersonService personService;


    @MockBean
    private PersonDao personDao;

    @Before
    public void setUp() {

        String passwordHash;
        try {
            Optional<Method> optionalMethod = Arrays.stream(PersonService.class.getDeclaredMethods())
                    .peek(method -> method.setAccessible(true))
                    .filter(method -> method.getName().equalsIgnoreCase("encryptPassword"))
                    .findFirst();

            passwordHash = (String) optionalMethod.orElseThrow(MethodNotFoundException::new).invoke(personService, "admin");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        Person admin = new Person("admin", passwordHash, "admin@sne,com", "admin", "admin");

        Mockito.when(personDao.getByUsername("admin")).thenReturn(admin);
        Mockito.when(personDao.getByUsername("")).thenReturn(null);
    }

    @Test
    public void handleEmptyRequest() {
        boolean result = personService.validateLogin("", "");
        assertFalse(result);
    }

    /**
     * Validating username/password
     * Positive test. If username/password is OK, this test should've been passed
     */
    @Test
    public void passwordChecking() {
        boolean result = personService.validateLogin("admin", "admin");
        assertTrue(result);
    }

}