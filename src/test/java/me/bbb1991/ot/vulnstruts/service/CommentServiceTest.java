package me.bbb1991.ot.vulnstruts.service;

import me.bbb1991.ot.vulnstruts.dao.CommentDao;
import me.bbb1991.ot.vulnstruts.dao.PostDao;
import me.bbb1991.ot.vulnstruts.model.Comment;
import me.bbb1991.ot.vulnstruts.model.Person;
import me.bbb1991.ot.vulnstruts.model.Post;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CommentService.class})
public class CommentServiceTest {

    @Autowired
    private CommentService commentService;

    @MockBean
    private CommentDao commentDao;

    @MockBean
    private PostService postService;

    private Comment comment;

    @Before
    public void setUp() {
        Person author = new Person();
        Post post = new Post(1, "Title", "Body", author);

        Mockito.when(postService.getPostById(post.getId())).thenReturn(Optional.of(post));

        comment = new Comment();
    }

    @Test
    public void testWrongPostId() {
        boolean result = commentService.saveComment(666, comment);
        assertFalse(result);
    }

}