# Проект для лабораторной работы по курсу Offensive Technologies
## Описание
По заданию, необходимо написать веб приложение с использованием 
Apache Struts c уязвимостью CVE-2017-5638.

## Ловушки, обманные ходы и др.
Если веб сайт будут сканировать разными тулзами, то всплывут информаций 
о разных уязвимых фреймворках и приложениях, которые на самом деле не
были использованы:
* **Wordpress v3.3.1** - 34 найденных уязвимостей
* Веб сервер: **Apache v2.3.6** - 7 найденных уязвимостей
* Web фреймворк: **Django**
* Кэширование: **Varnish**
* **Microsoft HTTPAPI**
* Модуль **OpenSSL v1.1.0a** - 10 найденных уязвимостей
* Язык программирования **PHP v5.5.31** - 8 найденных уязвимостей
* Операционная система **CentOS**


## Список работающих скрпитов и эксплоитов.
* [python](https://github.com/mazen160/struts-pwn)
* [bash](https://github.com/KarzsGHR/S2-046_S2-045_POC)
* [GO](https://github.com/Greynad/struts2-jakarta-inject)